import tweepy
import logging
import os
import json
import time
import func.api
import func.file
from random import randrange
from func.config import CONFIG
from func import video
from flask import Flask, request
from waitress import serve

POSTED_DATA_PATH = CONFIG["POSTED_FILES_STORAGE_PATH"] if CONFIG["ENV"] != "test" else "./data/TEST_POSTED_DATA.json"

# Controls interactions with the Twitter API
twitter_api = None
twitter_api_v2 = None

def getPostedData():
    return func.file.read(POSTED_DATA_PATH)

def initPostedData():
    DEFAULT_POSTED_DATA = { "posts": [], "last_post_time": None, "index": 1 }
    if not func.file.has(POSTED_DATA_PATH):
        func.file.write(POSTED_DATA_PATH, DEFAULT_POSTED_DATA)
    else:
        data = getPostedData()

        if not data.get("index"): data["index"] = 1

        if not data: func.file.write(POSTED_DATA_PATH, DEFAULT_POSTED_DATA)

def appendToPosted(id, post_time):
    data = getPostedData()

    data["posts"].append(id)
    data["last_post_time"] = post_time

    func.file.write(POSTED_DATA_PATH, data)

def getIndex() -> int:
    data = getPostedData()
    return data["index"]

def setIndex(idx) -> None:
    data = getPostedData()
    data["index"] = idx
    func.file.write(POSTED_DATA_PATH, data)
    #logging.info("Posting index set to: " + str(idx))

def get_tweet_text(data):
    return "{name}\n\nSource: {source}\n\nListen on RPG Radio: {link}\n\n#rpgmaker".format(name=data["name"], source="https://youtube.com/watch?v=" + data["path"],link=CONFIG["BASE_URL"] + "?track_id=" + data["_id"])

def post(idx, dry_run):
    try:
        data = func.api.get_post(idx).get("data")

        if not data:
            logging.error("Failed to get data for post with ID: " + str(idx))
            return None

        LOCAL_IMAGE_PATH = video.get(data)

        text = get_tweet_text(data)
        if not text:
            logging.error("Failed to get Tweet text for post " + str(data['_id']))
            return

        if not dry_run:
            res = twitter_api.media_upload(LOCAL_IMAGE_PATH, media_category="amplify_video")
            twitter_api_v2.create_tweet(text=text, media_ids=[res.media_id])
            # func.file.remove(LOCAL_IMAGE_PATH)

        current_time = time.time()
        logging.info(("(DRY RUN) " if dry_run else "") + "Tweeted post " + str(data["_id"]) + " at " + time.strftime("%D %H:%M:%S", time.localtime(current_time)) + ". Next Tweet is scheduled at " + time.strftime("%D %H:%M:%S", time.localtime(current_time + CONFIG["POST_INTERVAL"])))

        logging.info("Tweet text:\n" + text)

        if not dry_run:
            appendToPosted(data["_id"], current_time)
            setIndex(getIndex() + 1)

        return True
    except Exception as e:
        logging.error(str(e) + "\nFailed to tweet post: " + str(data["_id"]))
        return False

def hasPostIntervalPassed(posted_data) -> bool:
    if posted_data.get("last_post_time"):
        difference = int(time.time() - posted_data["last_post_time"])
        return difference > CONFIG["POST_INTERVAL"]
    return True

def checkForValid(idx, data) -> bool:
    is_valid_post = [i for i in range(len(data)) if data[i] == idx]
    return bool(is_valid_post)

def startPostLoop():
    logging.info("Starting post loop...")

    retry_count = 0

    while(True):
        all_valid_posts = func.api.get_all_valid_posts()
        if not all_valid_posts:
            logging.error("Failed to get all posts data. Sleeping for 3 seconds before retrying.")
            time.sleep(3)
            continue

        logging.info("Preparing to post...")
        posted_data = getPostedData()

        if not hasPostIntervalPassed(posted_data):
            logging.error("Not enough time has passed since the last post at " + time.strftime("%D %H:%M", time.localtime(posted_data["last_post_time"])))
            next_post_time = posted_data["last_post_time"] + CONFIG["POST_INTERVAL"]
            sleep_time = next_post_time - time.time()
            logging.info("Sleeping for " + time.strftime("%Hh%Mm", time.gmtime(sleep_time)) + " until the next post time at " + time.strftime("%D %H:%M", time.localtime(next_post_time)))
            time.sleep(sleep_time)
            continue

        id_to_post = all_valid_posts[posted_data["index"]]

        if id_to_post in posted_data["posts"]:
            logging.error("Post {id} is already posted. Skipping to {next_id}".format(id=id_to_post, next_id=posted_data["index"]+1))
            setIndex(posted_data["index"]+1)
            continue

        is_successfully_tweeted = post(id_to_post, CONFIG["ENV"] == "test")

        if not is_successfully_tweeted:
            if retry_count >= CONFIG["TWEET_RETRY_COUNT"]:
                retry_count = 0
                logging.error("Reached max retries count while trying to tweet post {id}. Skipping to {next_id}...".format(id=str(id_to_post), next_id=str(id_to_post+1)))
                setIndex(id_to_post + 1)
                continue
            else:
                logging.error("Will retry to tweet post {idx} in 3 seconds".format(idx=id_to_post))
                time.sleep(3)
                retry_count += 1
                continue

        time.sleep(CONFIG["POST_INTERVAL"])

def authenticate(callback):
    global twitter_api
    global twitter_api_v2
    auth = tweepy.OAuthHandler(CONFIG["API_KEY"], CONFIG["API_KEY_SECRET"])

    # Authenticate the Twitter user
    if CONFIG.get("ACCESS_TOKEN") and CONFIG.get("ACCESS_TOKEN_SECRET"):
        auth.set_access_token(CONFIG["ACCESS_TOKEN"], CONFIG["ACCESS_TOKEN_SECRET"])

        twitter_api = tweepy.API(auth)
        twitter_api_v2 = tweepy.Client(
            consumer_key = CONFIG["API_KEY"],
            consumer_secret = CONFIG["API_KEY_SECRET"],
            access_token = CONFIG["ACCESS_TOKEN"],
            access_token_secret = CONFIG["ACCESS_TOKEN_SECRET"],
        )

        logging.info("Authenticated from defined access tokens in config file")
        callback()
    elif os.path.isfile(CONFIG["ACCESS_TOKEN_STORAGE_PATH"]):
        with open(CONFIG["ACCESS_TOKEN_STORAGE_PATH"], "r") as f:
            data = json.load(f)
            if data.get("access_token") and data.get("access_token_secret"):
                auth.set_access_token(data["access_token"], data["access_token_secret"])
                twitter_api = tweepy.API(auth)
                f.close()
                logging.info("Authenticated from stored access tokens")
                callback()
            else:
                logging.error("Stored tokens are invalid")
                f.close()
                quit()
    else:
        logging.info("Running auth server at " + CONFIG["HOSTNAME"] + " on port " + CONFIG["PORT"])

        app = Flask(__name__)

        @app.route("/oauth")
        def verifyOauth():
            global twitter_api
            if twitter_api:
                # HACK: It'd be great to stop the Flask web server after authentication, but right now
                # we're just disabling this route once finished
                return "You are already authenticated"

            verifier = request.args.get("oauth_verifier")

            auth.get_access_token(verifier)

            # Store the user's access tokens
            with open(CONFIG["ACCESS_TOKEN_STORAGE_PATH"], "w+") as f:
                data = {
                    "access_token": auth.access_token,
                    "access_token_secret": auth.access_token_secret
                }
                json.dump(data, f)

            logging.info("Successfully authenticated user")

            twitter_api = tweepy.API(auth)

            return "You have been successfully authenticated. Restart the script to start posting from your bot."

        redirect_url = auth.get_authorization_url(False)
        print("Please go to " + redirect_url + " to authorize the application")
        serve(app, host=CONFIG["HOSTNAME"], port=CONFIG["PORT"])
