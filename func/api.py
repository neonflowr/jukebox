import requests
import logging
import time
import sys
from func.config import CONFIG
from func import cache

def request_api(url):
    retries = 0

    while retries < CONFIG["RETRY_COUNT"]:
        try:
            return requests.get(url = url, 
                timeout = CONFIG["REST_API_TIMEOUT"],
                auth = (CONFIG["MASADA_USERNAME"], CONFIG["MASADA_PASSWORD"]),
                headers = {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
            }).json()
        except:
            logging.error("Request_api threw an error:", sys.exc_info()[0])
            retries = retries + 1
            time.sleep(CONFIG["RETRY_TIMEOUT"])

    return None

def get_post(id):
    retries = 0

    while retries < CONFIG["RETRY_COUNT"]:
        try:
            return requests.post(url = CONFIG["BASE_URL"] + '/track/id', json = { "id" : id },
                timeout = CONFIG["REST_API_TIMEOUT"],
                auth = (CONFIG["MASADA_USERNAME"], CONFIG["MASADA_PASSWORD"]),
                headers = {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
            }).json()
        except:
            logging.error("Request_api threw an error:", sys.exc_info()[0])
            retries = retries + 1
            time.sleep(CONFIG["RETRY_TIMEOUT"])

    return None

def update_post_data():
    data = request_api(CONFIG["BASE_URL"] + "/track/all")
    data = list(map(lambda item: item["_id"], data))

    cache.insert("all_posts", data, time.time() + CONFIG.get("ALL_POSTS_CACHE_TIME"))

    return data
    
def get_all_valid_posts():
    cached_posts = cache.get("all_posts")
    if cached_posts:
        logging.info("Using cached post data...")
        return cached_posts
    
    return update_post_data()
